# User script "Wikipedia 2 Wikiwand (Global)"

* I always had a bad feeling about the official Wikiwand addon for Firefox that it is dragging back the performance of the browser. Then I found [Wikipedia 2 Wikiwand](https://greasyfork.org/zh-TW/scripts/13400-wikipedia-2-wikiwand "Wikipedia 2 Wikiwand") by [drhouse](https://greasyfork.org/zh-TW/users/10118-drhouse "drhouse") which seems to be lightweight and efficient working with Greasemonkey. So here is a fork/expansion to make that script applicable to most languages of Wikipedia (hopefully).
* The second line of the code is for users in Taiwan particularly. Feel free to change the "zh-tw" language code to whatever you like :-)
* [BitBucket repository](https://bitbucket.org/GHSROBERT/userscript-wikipedia2wikiwand-global) available. Please raise your support request there.

----------

!["Wikiwand" on Wikiwand](https://i.imgur.com/1uKs5TX.png)
The entry "Wikiwand" on Wikiwand

